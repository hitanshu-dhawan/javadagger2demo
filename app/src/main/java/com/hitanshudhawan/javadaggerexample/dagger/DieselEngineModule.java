package com.hitanshudhawan.javadaggerexample.dagger;

import com.hitanshudhawan.javadaggerexample.car.DieselEngine;
import com.hitanshudhawan.javadaggerexample.car.Engine;

import dagger.Module;
import dagger.Provides;

@Module
public class DieselEngineModule {

    private int horsePower;

    public DieselEngineModule(int horsePower) {
        this.horsePower = horsePower;
    }

    @Provides
    int provideHorsePower() {
        return horsePower;
    }

    @Provides
    Engine provideEngine(DieselEngine dieselEngine) {
        return dieselEngine;
    }

}
