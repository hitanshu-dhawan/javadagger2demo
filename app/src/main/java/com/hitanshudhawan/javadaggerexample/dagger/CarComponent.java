package com.hitanshudhawan.javadaggerexample.dagger;

import com.hitanshudhawan.javadaggerexample.MainActivity;
import com.hitanshudhawan.javadaggerexample.car.Car;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Members-injection methods have a single parameter and inject dependencies into each of
 * the Inject-annotated fields and methods of the passed instance.
 * A members-injection method may be void or return its single parameter as a convenience for chaining.
 * The following are all valid members-injection method declarations:
 * <p>
 * void injectSomeType(SomeType someType);
 * SomeType injectAndReturnSomeType(SomeType someType);
 */

@Singleton
@Component(modules = {WheelsModule.class, PetrolEngineModule.class})
public interface CarComponent {

    void injectMainActivity(MainActivity mainActivity);

    Car getCar();

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder horsePower(@Named("horsePower") int horsePower);

        @BindsInstance
        Builder engineCapacity(@EngineCapacity int engineCapacity);

        CarComponent build();

    }

}
