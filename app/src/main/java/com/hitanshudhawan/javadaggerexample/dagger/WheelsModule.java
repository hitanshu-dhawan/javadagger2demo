package com.hitanshudhawan.javadaggerexample.dagger;

import com.hitanshudhawan.javadaggerexample.car.Rims;
import com.hitanshudhawan.javadaggerexample.car.Tyres;
import com.hitanshudhawan.javadaggerexample.car.Wheels;

import dagger.Module;
import dagger.Provides;

@Module
public class WheelsModule {

    @Provides
    static Wheels provideWheels(Rims rims, Tyres tyres) {
        return new Wheels(rims, tyres);
    }

    @Provides
    static Rims provideRims() {
        return new Rims();
    }

    @Provides
    static Tyres provideTyres() {
        Tyres tyres = new Tyres();
        tyres.inflate();
        return tyres;
    }

}
