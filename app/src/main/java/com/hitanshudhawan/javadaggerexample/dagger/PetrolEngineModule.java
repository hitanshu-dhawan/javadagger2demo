package com.hitanshudhawan.javadaggerexample.dagger;

import com.hitanshudhawan.javadaggerexample.car.Engine;
import com.hitanshudhawan.javadaggerexample.car.PetrolEngine;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class PetrolEngineModule {

    @Binds
    abstract Engine bindEngine(PetrolEngine petrolEngine);

}
