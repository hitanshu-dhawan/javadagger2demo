package com.hitanshudhawan.javadaggerexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.hitanshudhawan.javadaggerexample.car.Car;
import com.hitanshudhawan.javadaggerexample.dagger.CarComponent;
import com.hitanshudhawan.javadaggerexample.dagger.DaggerCarComponent;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    // https://www.youtube.com/playlist?list=PLrnPJCHvNZuA2ioi4soDZKz8euUQnJW65

    // TODO
    // Dagger 2 Tutorial Part 11 - CUSTOM SCOPES & COMPONENT DEPENDENCIES - Android Studio Tutorial
    // https://youtu.be/V-CF0BGA-3w

    @Inject
    public Car car1, car2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CarComponent carComponent = DaggerCarComponent.builder()
                .horsePower(300)
                .engineCapacity(400)
                .build();
        carComponent.injectMainActivity(this);

        car1.drive();
        car2.drive();
    }

}
