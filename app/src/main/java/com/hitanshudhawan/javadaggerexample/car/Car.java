package com.hitanshudhawan.javadaggerexample.car;

import android.util.Log;

import javax.inject.Inject;

public class Car {

    private Engine engine;
    private Wheels wheels;

    private Driver driver;

    @Inject
    public Car(Engine engine, Wheels wheels, Driver driver) {
        this.engine = engine;
        this.wheels = wheels;

        this.driver = driver;
    }

    public void drive() {
        engine.start();
        Log.d("HITANSHUU", driver + " is driving " + this);
    }

}
