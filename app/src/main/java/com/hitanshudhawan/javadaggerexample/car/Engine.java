package com.hitanshudhawan.javadaggerexample.car;

public interface Engine {

    void start();

}
