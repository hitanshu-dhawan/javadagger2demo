package com.hitanshudhawan.javadaggerexample.car;

import android.util.Log;

import javax.inject.Inject;

public class DieselEngine implements Engine {

    private Block block;
    private Cylinders cylinders;
    private SparkPlugs sparkPlugs;

    private int horsePower;

    @Inject
    public DieselEngine(Block block, Cylinders cylinders, SparkPlugs sparkPlugs, int horsePower) {
        this.block = block;
        this.cylinders = cylinders;
        this.sparkPlugs = sparkPlugs;

        this.horsePower = horsePower;
    }

    @Override
    public void start() {
        Log.d("HITANSHUU", "DieselEngine started with horsepower " + horsePower);
    }

}
