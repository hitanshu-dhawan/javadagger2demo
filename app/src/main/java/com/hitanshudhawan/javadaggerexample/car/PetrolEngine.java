package com.hitanshudhawan.javadaggerexample.car;

import android.util.Log;

import com.hitanshudhawan.javadaggerexample.dagger.EngineCapacity;

import javax.inject.Inject;
import javax.inject.Named;

public class PetrolEngine implements Engine {

    private Block block;
    private Cylinders cylinders;
    private SparkPlugs sparkPlugs;

    private int horsePower;
    private int engineCapacity;

    @Inject
    public PetrolEngine(Block block, Cylinders cylinders, SparkPlugs sparkPlugs, @Named("horsePower") int horsePower, @EngineCapacity int engineCapacity) {
        this.block = block;
        this.cylinders = cylinders;
        this.sparkPlugs = sparkPlugs;

        this.horsePower = horsePower;
        this.engineCapacity = engineCapacity;
    }

    @Override
    public void start() {
        Log.d("HITANSHUU", "PetrolEngine started with horsepower " + horsePower + " and engine capacity " + engineCapacity);
    }

}
