package com.hitanshudhawan.javadaggerexample.car;

/**
 * Consider Wheels as a 3rd party class, so we can't use @Inject annotation here.
 * So, we use @Module and @Provides.
 */

public class Wheels {

    private Rims rims;
    private Tyres tyres;

    public Wheels(Rims rims, Tyres tyres) {
        this.rims = rims;
        this.tyres = tyres;
    }

}
