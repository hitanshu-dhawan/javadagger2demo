# Dagger2 (Java)

## Articles/Videos

Series of articles by Hari Vignesh Jayapalan with Game of Throne examples.  
https://medium.com/@harivigneshjayapalan/dagger-2-for-android-beginners-introduction-be6580cb3edb

Awesome YouTube video series. Explaned slowly and in-depth.  
https://www.youtube.com/playlist?list=PLrnPJCHvNZuA2ioi4soDZKz8euUQnJW65

Official Dagger documentation. Good, detailed and full of knowledge.  
https://google.github.io/dagger/users-guide  
https://google.github.io/dagger/android.html

Best Practices of using Dagger2  
https://medium.com/square-corner-blog/keeping-the-daggers-sharp-%EF%B8%8F-230b3191c3f

Talk by Jake Wharton  
https://youtu.be/plK0zyRLIP8

----
## TODO Reads/Talks
https://proandroiddev.com/dagger-2-on-android-the-simple-way-f706a2c597e9  
https://medium.com/@Zhuinden/that-missing-guide-how-to-use-dagger2-ef116fbea97  
https://youtu.be/oK_XtfXPkqw  
----

----
### Todo: MindOrks list of resources  
https://blog.mindorks.com/a-complete-guide-to-learn-dagger-2-b4c7a570d99c  
#### List
1. https://blog.mindorks.com/introduction-to-dagger-2-using-dependency-injection-in-android-part-1-223289c2a01b
2. https://blog.mindorks.com/introduction-to-dagger-2-using-dependency-injection-in-android-part-2-b55857911bcd
3. https://blog.mindorks.com/the-new-dagger-2-android-injector-cbe7d55afa6a
4. http://frogermcs.github.io/dependency-injection-with-dagger-2-introdution-to-di/
5. http://frogermcs.github.io/dependency-injection-with-dagger-2-the-api/
6. https://blog.mindorks.com/android-dagger2-critical-things-to-know-before-you-implement-275663aecc3e
7. https://www.youtube.com/watch?v=-UWWFBEhW3Q (Dagger 2's Codegen implementation)
8. https://fernandocejas.com/2015/04/11/tasting-dagger-2-on-android/
----